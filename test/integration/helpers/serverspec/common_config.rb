#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'spec_helper'

describe 'Spark Configuration' do
  describe file('/opt/spark/conf/log4j.properties') do
    its(:content) { should contain 'spark-project.jetty=INFO' }
    its(:content) { should contain 'log4j.logger.org.apache.parquet=ERROR' }
  end

  describe file('/opt/spark/conf/spark-env.sh') do
    its(:content) { should contain 'SPARK_LOCAL_DIRS=/tmp/spark' }
    its(:content) { should contain 'SPARK_MASTER_PORT=7077' }
  end

  describe file('/opt/spark/conf/metrics.properties') do
    its(:content) { should contain 'metrics.sink.JmxSink' }
  end
end
