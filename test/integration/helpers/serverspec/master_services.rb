#
# Copyright (c) 2016 Sam4Mobile
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'common_config'

describe 'Spark master' do
end

curl = 'http_proxy="" curl -sS -X GET'
alive = '<li><strong>Status:</strong> ALIVE</li>'
standby = '<li><strong>Status:</strong> STANDBY</li>'
masters = (1..2).map { |i| "spark-platform-#{i}-centos-7" }
master_ui_port = 8080

def workers(n)
  "<li><strong>Alive Workers:</strong> #{n}</li>"
end

def wait_workers(curl, master)
  (1..20).each do |try|
    http = `#{curl} #{master}`
    break if http.include?(workers(3))
    puts "Waiting for workers to join… Try ##{try}/20, waiting 5s"
    sleep(5)
  end
end

def get_status(curl, masters, master_ui_port, alive, standby)
  masters.map.with_index do |url, i|
    http = `#{curl} http://#{url}:#{master_ui_port}`
    if http.include?(alive)
      { alive: i }
    elsif http.include?(standby)
      { standby: i }
    else
      { none: i }
    end
  end.reduce(:merge)
end

status = []
(1..20).each do |try|
  status = get_status(curl, masters, master_ui_port, alive, standby)
  break if status.keys.sort == [:alive, :standby]
  puts "Waiting for leader election… Try ##{try}/20, waiting 5s"
  sleep(5)
end

describe 'Spark masters:' do # rubocop:disable BlockLength
  it 'service is running' do
    expect(service('spark-master')).to be_running
  end

  it 'service is launched at boot' do
    expect(service('spark-master')).to be_enabled
  end

  it 'are listening for worker connections' do
    expect(port(7077)).to be_listening
  end

  it 'have ui listening on correct port' do
    expect(port(8080)).to be_listening
  end

  it 'one is alive, the other is in stand by' do
    expect(status.keys.sort).to eq([:alive, :standby])
  end

  unless status[:alive].nil?
    wait_workers(curl, "http://#{masters[status[:alive]]}:#{master_ui_port}")
    it 'active master has 3 workers' do
      result = `#{curl} http://#{masters[status[:alive]]}:#{master_ui_port}`
      expect(result).to include(workers(3))
    end
  end

  unless status[:standby].nil?
    it 'stand-by master has 0 worker' do
      result = `#{curl} http://#{masters[status[:standby]]}:#{master_ui_port}`
      expect(result).to include(workers(0))
    end
  end
end

spark_bin = '/opt/spark/bin/spark-submit'
connect = masters.map { |m| "#{m}:7077" }.join(',')
spark_submit = "#{spark_bin} --master=spark://#{connect}"
spark_example = '/opt/spark/examples/src/main/python/pi.py 100'

describe 'Pi estimation' do
  unless status[:alive].nil?
    it 'is correct' do
      exp = 'Pi is roughly 3.1'
      result = `#{spark_submit} #{spark_example} 2> /dev/null`
      expect(result).to include(exp)
    end
  end
end
